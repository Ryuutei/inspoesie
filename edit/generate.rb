#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
require "json"
=begin ————————————————————————————————————————————————————————————————————————
　　　　　　　ＵＳＡＧＥ:: 
　ＤＥＳＣＲＩＰＴＩＯＮ:: crée la base de donnée, et l'enregistre au format json
ＤＥＰＥＮＤＥＮＣＩＥＳ:: 
　　　　　　ＡＵＴＨＯＲ:: Sebastien Blanc 龍帝〔ryuutei@gmail.com〕
　　　　ＲＥＶＩＳＩＯＮ:: 2
=end ——————————————————————————————————————————————————————————————————————————
a = open("liste_francais.txt", "r").read.split("\r\n")
# liste_francais.txt est trouvable encodé en LATIN1 sur: 
# http://www.freelang.com/dictionnaire/dic-français.php
# iconv -f latin1 -t utf-8 liste_francais.txt > liste_francais.utf8.txt
# ;)

res = []
a.each { |word|
    # Règles
    if word.length <=3 then next
    elsif word.end_with? "ais" then next
    elsif word.end_with? "ait" then next
    elsif word.end_with? "aient" then next
    elsif word.end_with? "ons" then next
    elsif word.end_with? "ez" then next

    # ajout du mot valide dans le resultat.
    else res << word
    end 
}

puts "#{a.length} VS #{res.length}"

open("../db.json", "w") {|f| f.write( { "dic" => res }.to_json ) }
