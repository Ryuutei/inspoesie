#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
require "json"
=begin ————————————————————————————————————————————————————————————————————————
　　　　　　　ＵＳＡＧＥ:: 
　ＤＥＳＣＲＩＰＴＩＯＮ:: reformate la base de donnée
ＤＥＰＥＮＤＥＮＣＩＥＳ:: 
　　　　　　ＡＵＴＨＯＲ:: Sebastien Blanc 龍帝〔ryuutei@gmail.com〕
　　　　ＲＥＶＩＳＩＯＮ:: 1
=end ——————————————————————————————————————————————————————————————————————————
$prettyPrint = true

$db = JSON.parse(open("../db.json").read)

if $prettyPrint then
    open("../db.json", "w") {|ƒ| ƒ.puts JSON.pretty_generate $db }
else
    open("../db.json", "w") {|ƒ| ƒ.puts $db.to_json }
end
