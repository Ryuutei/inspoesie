#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
=begin ————————————————————————————————————————————————————————————————————————
　　　　　　　ＵＳＡＧＥ:: 
　ＤＥＳＣＲＩＰＴＩＯＮ:: 
ＤＥＰＥＮＤＥＮＣＩＥＳ:: 
　　　　　　ＡＵＴＨＯＲ:: Sebastien Blanc 龍帝〔ryuutei@gmail.com〕
　　　　ＲＥＶＩＳＩＯＮ:: 1
=end ——————————————————————————————————————————————————————————————————————————
$sevenzip = "7za a -t7z -m0=lzma -mx=9 -mfb=64 -md=32m -ms=on"

if ARGV.length == 0 then
    %x{#{$sevenzip} inspoesie.7z db.json inspoesie.py}
else
    if ARGV[0].start_with?("exe") then
        # py2exe
        puts "todo"
    elsif ARGV[0].start_with?("7z") then
        %x{#{$sevenzip} inspoesie.7z db.json inspoesie.py}
    elsif ARGV[0].start_with?("all") then
        %x{#{$sevenzip} inspoesie.7z README.adoc db.json edit icon inspoesie.py}
    elsif ARGV[0].start_with?("ju") then
        #%x{cp    }
        %x{rsync -poghb --backup-dir=/tmp/rsync -e /dev/null --progress inspoesie.py /Users/ryuutei/Dropbox/Manatree/inspoesie/inspoesie.py}
        #%x{#{$sevenzip} inspoesie.7z db.json inspoesie.py}
        #%x{cp inspoesie.7z /Users/ryuutei/Dropbox/Manatree/}
    end
end
