#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# coding: utf8
import sys
import json
import random
import tkinter as tk
import locale
#———————————————————————————————————————————————————————————————————————————————
# Configuration
#———————————————————————————————————————————————————————————————————————————————
longueur_de_la_liste =  3
base_de_donnée =        "db.json"
#———————————————————————————————————————————————————————————————————————————————
#　　　　　　　ＵＳＡＧＥ：inspoesie.py
#　　　　　　　ＵＳＡＧＥ：inspoesie.py <nombre>
#　　　　　　　ＵＳＡＧＥ：inspoesie.py -gui
#　ＤＥＳＣＲＩＰＴＩＯＮ：crée une liste de 3 mots choisis aleatoirement dans les mots français.
#ＤＥＰＥＮＤＥＮＣＩＥＳ：python3
#　　　　　　ＡＵＴＨＯＲ：Sebastien Blanc 龍帝〔ryuutei@gmail.com〕
# TODO: http://www.py2exe.org
#———————————————————————————————————————————————————————————————————————————————
#__doc__     = """TODO"""
__enc__     = 'utf_8'
__version__ = "1"
__revision__= "3"

with open(base_de_donnée, "r") as f:
    db = json.loads(f.read(), encoding=__enc__)["dic"]
#———————————————————————————————————————————————————————————————————————————————
#print(locale.getpreferredencoding()) # debug

def main():
    if sys.platform == "darwin" or sys.platform == "linux" or sys.platform == "cygwin":
        print("Ins—poésie version {}.{}".format(__version__, __revision__))

        if len(sys.argv) == 1:
            print("{}".format("，".join(inspoésie(longueur_de_la_liste ))))
        else:
            if sys.argv[1] == "-gui": inspoégui()
            else:
                for ii in range( int(sys.argv[1]) ):
                    print("{}".format("，".join(inspoésie(longueur_de_la_liste ))))

    else: inspoégui()


def inspoésie(δ):
    """crée une liste de 3 mots différents choisis aleatoirement dans les mots français de la db.
    v.2

    :returns: `list`, liste de 3 mots.
    """
    res = [random.choice(db), ]

    while len(res) < δ:
        χt = random.choice(db)
        check = True
        for ii in res:
            if ii == χt:
                check = False

        if check: res.append( χt )
    return res

def inspoégui():
    app = Application()
    app.master.title( "Ins—poésie version {}.{}".format(__version__, __revision__))
    app.mainloop()

class Application(tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self, master, width="12c", height="7c",)
        #self.grid()
        self.grid(sticky=tk.N+tk.S+tk.E+tk.W)

        self.longueur = tk.IntVar(self, longueur_de_la_liste )
        self.résultat = tk.StringVar(self,   "{}".format(", ".join(inspoésie(self.longueur.get())))   )

        self.createWidgets()

    def régénère(self):
        """regénère la liste affichée."""
        self.résultat.set(   "{}".format(", ".join(inspoésie(self.longueur.get())))   )
        print(self.résultat.get())

    def createWidgets(self):
        top=self.winfo_toplevel()
        top.rowconfigure(0, weight=1)
        top.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)
        self.columnconfigure(0, weight=1)

        # 0,0
        #self.logo = tk.PhotoImage(self, file='./inspoesie256.gif')
        #self.logo.grid(row=0, column=0)
        # Fuck off I'll not add an image for now
        
        # 0,1 choix de la longueur de la liste
        self.barre = tk.Scale(self, label="Longueur de la liste", orient="horizontal", length="12c", digits=2, from_=2, to=16, variable=self.longueur  )
        self.barre.grid(row=0, column=1)
        # 0,2

        # 1,0

        # 1,1 résultat
        self.panneau = tk.Entry(self, state="readonly", justify="center", width=80, textvariable=self.résultat)
        self.panneau.grid(row=1, column=1, sticky=tk.E+tk.W)

        # debug
        self.panno = tk.Label(self, textvariable= tk.StringVar(self, "locale: " + str(locale.getdefaultlocale()) ))
        self.panno.grid(row=2, column=1, sticky=tk.E+tk.W)

        # 2,2 résultat
        self.encore = tk.Button(self, text='Encor！', command=self.régénère)
        self.encore.grid(row=2, column=2, sticky=tk.N+tk.E+tk.W)

        # 2,1
        # 2,0 Quit
        self.quit = tk.Button(self, text='Quitter', command=self.quit)
        self.quit.grid(row=2, column=0)


if __name__  == "__main__":
    version = __version__
    #inspoégui() # debug
    main()
